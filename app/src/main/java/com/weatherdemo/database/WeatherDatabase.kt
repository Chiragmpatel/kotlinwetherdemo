package com.weatherdemo.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.weatherdemo.data.dao.CityDao
import com.weatherdemo.data.entity.CityItem

@Database(entities = [CityItem::class], version = 1,exportSchema = false)
abstract class WeatherDatabase : RoomDatabase() {

    abstract fun cityDataDao(): CityDao

    companion object {

        private var INSTANCE: WeatherDatabase? = null

        fun getInstance(context: Context): WeatherDatabase? {
            if(INSTANCE == null){
                synchronized(WeatherDatabase::class){
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            WeatherDatabase::class.java, "weather.db")
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }

    }

}