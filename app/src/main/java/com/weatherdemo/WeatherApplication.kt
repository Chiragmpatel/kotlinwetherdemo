package com.weatherdemo

import android.app.Application
import com.weatherdemo.database.WeatherDatabase
import io.reactivex.internal.functions.Functions
import io.reactivex.plugins.RxJavaPlugins



class WeatherApplication : Application() {


    companion object {
         var INSTANCE : WeatherApplication? = null
    }

    override fun onCreate() {
        super.onCreate()
        if(INSTANCE == null){
            INSTANCE = this
        }

        RxJavaPlugins.setErrorHandler(Functions.emptyConsumer())
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
    }

    override fun onTerminate() {
        super.onTerminate()
        WeatherDatabase.destroyInstance()
    }
}