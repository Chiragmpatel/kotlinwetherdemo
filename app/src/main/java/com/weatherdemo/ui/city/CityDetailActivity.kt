package com.weatherdemo.ui.city

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.weatherdemo.R
import com.weatherdemo.data.entity.CityItem
import com.weatherdemo.data.entity.WeatherModel
import com.weatherdemo.databinding.ActivityCityDetailBinding
import kotlinx.android.synthetic.main.activity_city_detail.*
class CityDetailActivity : AppCompatActivity(), LifecycleOwner {

    private lateinit var viewModel: CityDetailViewModel
    private var cityItem: CityItem? = null

    companion object {

        const val CITY_ITEM = "city_item"

// Just to know have created this in either way like we can navigate activity by creating this method in destination activity as well
        fun route(context: Context, cityItem: CityItem) {
            val intent = Intent(context, CityDetailActivity::class.java)
            intent.putExtra(CITY_ITEM, cityItem)
            ContextCompat.startActivity(context, intent, null)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        cityItem = intent?.getParcelableExtra(CITY_ITEM)


        val binding = DataBindingUtil.setContentView(
                this, R.layout.activity_city_detail) as ActivityCityDetailBinding

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        viewModel = ViewModelProviders.of(this).get(CityDetailViewModel::class.java)
        viewModel.getWeatherModel().observe(this, Observer<WeatherModel> {
            binding.weatherItem = it
            binding.executePendingBindings()
        })

        cityItem?.let {
            viewModel.getWeatherData(it.latitude ?: 0.0, it.longitude ?: 0.0)
        }


    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId == android.R.id.home){
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
