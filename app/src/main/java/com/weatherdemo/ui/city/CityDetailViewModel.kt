package com.weatherdemo.ui.city

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.weatherdemo.data.entity.WeatherModel
import com.weatherdemo.service.rest.ServiceGenerator
import com.weatherdemo.service.rest.WeatherAPI
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CityDetailViewModel : ViewModel() {

    private var weatherModel : MutableLiveData<WeatherModel>? = null

    fun getWeatherModel() : MutableLiveData<WeatherModel>{
        if(weatherModel == null){
            weatherModel = MutableLiveData()
        }
        return weatherModel!!
    }

    fun getWeatherData(latitude: Double, longitude: Double){

                ServiceGenerator.createService(WeatherAPI::class.java).getTodaysWeather(latitude,longitude)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    weatherModel?.value = it
                },{
                    Log.e("ERROR","${it.message}")
                })
    }


}