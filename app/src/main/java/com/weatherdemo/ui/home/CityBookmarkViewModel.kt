package com.weatherdemo.ui.home

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.location.Address
import android.location.Geocoder
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import com.weatherdemo.WeatherApplication
import com.weatherdemo.data.entity.CityItem
import com.weatherdemo.database.WeatherDatabase
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

class CityBookmarkViewModel : ViewModel(){

    private var bookmarkCityList: MutableLiveData<List<CityItem>>? = null

    fun getbookmarkCities() : MutableLiveData<List<CityItem>>{
        if(bookmarkCityList == null){
            bookmarkCityList = MutableLiveData()
            loadBookmarkCities()
        }
        return bookmarkCityList!!
    }

    private fun loadBookmarkCities(){
        WeatherDatabase.getInstance(WeatherApplication.INSTANCE?.applicationContext!!)
                ?.cityDataDao()
                ?.getAllBookmarkedCity()
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe {
                    bookmarkCityList?.value = it
                }
    }

    fun insertLocationToDb( it: LatLng?) {


        Observable.fromCallable {

            val geocoder = Geocoder(WeatherApplication.INSTANCE,Locale.getDefault())
            val addressesList =  geocoder.getFromLocation(it?.latitude?:0.0, it?.longitude?:0.0,1) as List<Address>
            val addresses = addressesList.get(0)
            var cityName = addresses.adminArea?:addresses.subAdminArea
            var countryName = addresses?.countryName
            val sb = StringBuilder()
            sb.append(cityName).append(", ").append(countryName)
            val address = sb.toString()
            val latitude = addresses?.latitude
            val longitude = addresses?.longitude

            WeatherDatabase.getInstance(WeatherApplication.INSTANCE?.applicationContext!!)
                    ?.cityDataDao()
                    ?.bookmarkCity(CityItem(id = null,
                            cityName = address,
                            latitude =  latitude,
                            longitude = longitude))
        } ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe {
                   Log.e("TAG","Data inserted $it")
                }

    }

    fun deleteCityFromBookmark(city: CityItem?) {
        Observable.fromCallable {
            WeatherDatabase.getInstance(WeatherApplication.INSTANCE?.applicationContext!!)
                    ?.cityDataDao()
                    ?.deleteCity(city!!)
        }
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    Log.e("TAG","Data inserted $it")
                },{
                    Log.e("ERROR","${it.message}")
                })
    }


}
