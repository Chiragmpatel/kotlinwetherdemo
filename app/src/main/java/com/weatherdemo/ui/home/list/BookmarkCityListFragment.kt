package com.weatherdemo.ui.home.list

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.weatherdemo.R
import com.weatherdemo.common.RecyclerItemClickListener
import com.weatherdemo.data.entity.CityItem
import com.weatherdemo.ui.city.CityDetailActivity
import com.weatherdemo.ui.home.CityBookmarkViewModel
import kotlinx.android.synthetic.main.fragment_bookmark.*

class BookmarkCityListFragment : Fragment(), LifecycleOwner {

    private lateinit var cityBookmarkViewModel: CityBookmarkViewModel
    private var cityListAdapter: BookmarkCityListAdapter? = null

    private val itemTouchHelper = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        override fun onMove(p0: RecyclerView, p1: RecyclerView.ViewHolder, p2: RecyclerView.ViewHolder): Boolean {
            return false
        }

        override fun onSwiped(p0: RecyclerView.ViewHolder, p1: Int) {
            cityBookmarkViewModel.deleteCityFromBookmark(cityListAdapter?.getItemAt(p0.adapterPosition))
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        cityBookmarkViewModel = activity?.run {
            ViewModelProviders.of(this).get(CityBookmarkViewModel::class.java)
        } ?: throw Exception("Invalid activity")


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bookmark, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cityBookmarkViewModel.getbookmarkCities().observe(this, Observer<List<CityItem>> {
            cityListAdapter?.setListData(it)
        })

        cityListAdapter = BookmarkCityListAdapter(object : RecyclerItemClickListener {
            override fun onItemClick(pos: Int, item: Any?, view: View) {
                Log.e("DATA", "ITEM : $item, POS: $pos ")
                CityDetailActivity.route(context!!,item as CityItem)
            }
        })
        bookmarkRV.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        bookmarkRV.isNestedScrollingEnabled = false
        bookmarkRV.adapter = cityListAdapter
        bookmarkRV.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        ItemTouchHelper(itemTouchHelper).attachToRecyclerView(bookmarkRV)
    }

}