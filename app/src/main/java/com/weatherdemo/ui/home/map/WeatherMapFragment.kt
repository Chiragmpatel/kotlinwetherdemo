package com.weatherdemo.ui.home.map

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.weatherdemo.R
import com.weatherdemo.data.entity.CityItem
import com.weatherdemo.ui.home.CityBookmarkViewModel


class WeatherMapFragment : Fragment(),LifecycleOwner, OnMapReadyCallback {

    private var map: GoogleMap? = null
    private var locationObserver: LocationObserver? = null

    private var myLocationMarker: Marker? = null

    private lateinit var cityBookmarkViewModel: CityBookmarkViewModel


    companion object {
        const val LOCATION_REQUEST_CODE = 1
        const val LOCATION_PERMISSION = android.Manifest.permission.ACCESS_FINE_LOCATION
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        locationObserver = LocationObserver(context!!,lifecycle){
            latLng -> showCurrentLocationOnMap(latLng)
        }
        lifecycle.addObserver(locationObserver!!)

        cityBookmarkViewModel = activity?.run {
            ViewModelProviders.of(this).get(CityBookmarkViewModel::class.java)
        }?:throw Exception("Invalid activity")
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

    return inflater.inflate(R.layout.fragment_weather_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mapFragment = childFragmentManager.findFragmentById(R.id.googleMap) as SupportMapFragment
        mapFragment.getMapAsync(this)

        if(ContextCompat.checkSelfPermission(context!!, LOCATION_PERMISSION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(activity!!, arrayOf(LOCATION_PERMISSION), LOCATION_REQUEST_CODE)
        }else{
            locationObserver?.enableLocation()
        }

        cityBookmarkViewModel.getbookmarkCities().observe(this, Observer<List<CityItem>> {
            map?.clear()
            it?.forEach {
                city -> map?.addMarker(MarkerOptions()
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
                        .position(LatLng(city.latitude!!, city.longitude!!)).title(city.cityName))
            }


        })
    }


    private fun showCurrentLocationOnMap(latLng: LatLng) {
        map?.let {
            myLocationMarker?.remove()
            myLocationMarker = it.addMarker(MarkerOptions().position(latLng)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                    .title("My Location"))
        }
    }


    override fun onMapReady(p0: GoogleMap?) {
      this.map = p0
        map?.setOnMapClickListener {
            map?.addMarker(MarkerOptions()
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET))
                    .position(it))

            cityBookmarkViewModel.insertLocationToDb(it)
        }

        map?.setOnMarkerClickListener {
        if(it.isInfoWindowShown) it.hideInfoWindow() else it.showInfoWindow()
            map?.animateCamera(CameraUpdateFactory.newLatLngZoom(it.position,15f))
            return@setOnMarkerClickListener true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(grantResults.isNotEmpty()){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                locationObserver?.enableLocation()
            }
        }
    }

}