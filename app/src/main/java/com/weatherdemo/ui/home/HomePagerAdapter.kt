package com.weatherdemo.ui.home

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.weatherdemo.ui.home.list.BookmarkCityListFragment
import com.weatherdemo.ui.home.map.WeatherMapFragment

class HomePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(pos: Int): Fragment? {
        return when (pos) {
            0 -> WeatherMapFragment()
            1 -> BookmarkCityListFragment()
            else -> null
        }
    }

    override fun getCount() = 2

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Map"
            1 -> "Bookmark"
            else -> ""
        }
    }
}