package com.weatherdemo.ui.home.map

import android.annotation.SuppressLint
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng

class LocationObserver(private val context: Context,
                       private val lifecycle: Lifecycle,
                       private val callback: (LatLng) -> Unit) : LifecycleObserver {

    private var fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)

    private var isLocationPermissionGiven = false

    private val locationRequest = LocationRequest().apply {
        fastestInterval = 10000
        interval = 60000
        priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY

    }

    private var locationCallback = object : LocationCallback() {

        override fun onLocationResult(p0: LocationResult?) {
            p0?.locations?.forEach {
                callback.invoke(LatLng(it.latitude,it.longitude))
            }
        }

        override fun onLocationAvailability(p0: LocationAvailability?) {
            super.onLocationAvailability(p0)
        }
    }

    @SuppressLint("MissingPermission")
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun startLocationRequest(){
        if(isLocationPermissionGiven){
            fusedLocationProviderClient?.requestLocationUpdates(locationRequest, locationCallback, null)
        }

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun stopLocationRequest(){
        fusedLocationProviderClient?.removeLocationUpdates(locationCallback)
    }

    fun enableLocation(){
             isLocationPermissionGiven = true
            startLocationRequest()
    }


}