package com.weatherdemo.ui.home.list

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.weatherdemo.R
import com.weatherdemo.common.RecyclerItemClickListener
import com.weatherdemo.data.entity.CityItem
import com.weatherdemo.databinding.ItemBookmarkCityRowBinding

class BookmarkCityListAdapter(val onItemClickListener: RecyclerItemClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var list: MutableList<CityItem> = mutableListOf()


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate(LayoutInflater.from(p0.context), R.layout.item_bookmark_city_row, p0, false) as ItemBookmarkCityRowBinding
        return BookmarkCityHolder(binding)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        (p0 as? BookmarkCityHolder)?.bind(p1)
    }

    fun setListData(it: List<CityItem>?) {
        it?.let {

            list.clear()
            list.addAll(it)
            notifyDataSetChanged()
        }
    }

    fun getItemAt(adapterPosition: Int): CityItem {
        return list[adapterPosition]
    }

    inner class BookmarkCityHolder(val binding: ItemBookmarkCityRowBinding)
        : RecyclerView.ViewHolder(binding.root) {

        fun bind(position: Int) {
            binding.cityItem = list[position]
            binding.root.setOnClickListener {
                onItemClickListener.onItemClick(position, list[position], it)
            }
        }
    }
}