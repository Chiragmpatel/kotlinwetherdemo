package com.weatherdemo.common

import android.view.View

interface RecyclerItemClickListener {

    fun onItemClick(pos: Int, item: Any?,view: View)
}