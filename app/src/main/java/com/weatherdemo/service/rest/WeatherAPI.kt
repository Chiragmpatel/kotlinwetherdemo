package com.weatherdemo.service.rest

import com.weatherdemo.data.entity.WeatherModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherAPI {

    @GET(RestUtil.APIMethods.GET_TODAY_WEATHER)
    fun getTodaysWeather(
            @Query(RestUtil.APIMethods.QUERY_LAT) lat: Double?,
            @Query(RestUtil.APIMethods.QUERY_LONG) lon: Double?,
            @Query(RestUtil.APIMethods.QUERY_APPID) appid: String? = RestUtil.WEATHER_APP_ID ,
            @Query(RestUtil.APIMethods.QUERY_UNIT) units: String? = RestUtil.WEATHER_UNIT

            ) : Single<WeatherModel>

}