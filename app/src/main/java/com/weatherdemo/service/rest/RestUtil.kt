package com.weatherdemo.service.rest

import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import java.io.IOException

object RestUtil {
    var jsonMapper = ObjectMapper().apply {
        enable(SerializationFeature.INDENT_OUTPUT)
// to allow serialization of "empty" POJOs (no properties to serialize)
// (without this setting, an exception is thrown in those cases)
        disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
// to write java.util.Date, Calendar as number (timestamp):
        disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)

// DeserializationFeature for changing how JSON is read as POJOs:

// to prevent exception when encountering unknown property:
        disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
// to allow coercion of JSON empty String ("") to null Object value:
        enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT)

        enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT)

        setVisibility(serializationConfig.defaultVisibilityChecker
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE))
    }
    const val WEATHER_APP_ID = "c6e381d8c7ff98f0fee43775817cf6ad"
    const val WEATHER_UNIT = "metric"

    fun getJSONFromObject(o: Any): String? {
        try {
            return jsonMapper.writeValueAsString(o)
        } catch (e: JsonProcessingException) {
            e.printStackTrace()
        }

        return null
    }

    fun getObjectFromJSON(aString: String, mObjectClass: Class<*>): Any? {
        try {
            return jsonMapper.readValue(aString, mObjectClass)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return null
    }


    object APIMethods {
        const val BASE_URL = "http://api.openweathermap.org/data/2.5/"
        const val GET_TODAY_WEATHER = "weather"
        const val GET_FORECAST_WEATHER = "forecast"

        const val QUERY_LAT = "lat"
        const val QUERY_LONG = "lon"
        const val QUERY_APPID = "appid"
        const val QUERY_UNIT = "units"
    }

}