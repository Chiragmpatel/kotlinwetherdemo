package com.weatherdemo.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.weatherdemo.data.entity.CityItem
import io.reactivex.Flowable

@Dao
interface CityDao {

    @Insert
    fun bookmarkCity(city: CityItem) : Long

    @Delete
    fun deleteCity(city: CityItem) : Int

    @Query("SELECT * FROM ${CityItem.CITY_TABLE}")
    fun getAllBookmarkedCity() : Flowable<List<CityItem>>
}