package com.weatherdemo.data.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.os.Parcelable
import com.weatherdemo.data.entity.CityItem.Companion.CITY_TABLE
import kotlinx.android.parcel.Parcelize


@Parcelize
@Entity(tableName = CITY_TABLE)
data class CityItem(

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = CITY_ID)
        @Bindable
        var id: Long? = null,

        @ColumnInfo(name = CITY_NAME)
        @Bindable
        var cityName: String? = null,


        @ColumnInfo(name = LATITUDE)
        @Bindable
        var latitude: Double? = null,

        @ColumnInfo(name = LONGITUDE)
        @Bindable
        var longitude: Double? = null


) : BaseObservable(), Parcelable {

    companion object {
        const val CITY_TABLE = "tbl_city"
        const val CITY_ID = "city_id"
        const val CITY_NAME = "city_name"
        const val LATITUDE = "latitude"
        const val LONGITUDE = "longitude"
    }


}
