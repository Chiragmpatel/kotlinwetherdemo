package com.weatherdemo.data.entity

import android.databinding.BaseObservable
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class WeatherModel (
        @field:JsonProperty("weather") val weather: List<WeatherItem> = emptyList(),
        @field:JsonProperty("main") val weatherMain: WeatherMainItem = WeatherMainItem(),
        @field:JsonProperty("visibility") val visibility: Long = 0L,
        @field:JsonProperty("base") val base: String = "",
        @field:JsonProperty("wind") val wind: WindItem = WindItem(),
        @field:JsonProperty("clouds") val cloud: CloudItem = CloudItem(),
        @field:JsonProperty("dt") val date: Date? = null,
        @field:JsonProperty("sys") val sysItem: SysItem = SysItem(),
        @field:JsonProperty("id") val id: Long = 0L,
        @field:JsonProperty("name") val name: String = "",
        @field:JsonProperty("cod") val cod: Int = 0
        ) : BaseObservable() {

    data class SysItem(
            @field:JsonProperty("type") val type: Int = 0,
            @field:JsonProperty("id") val id: Int = 0,
            @field:JsonProperty("message") val message: Double = 0.0,
            @field:JsonProperty("country") val country: String= "",
            @field:JsonProperty("sunrise") val sunrise: Date? = null,
            @field:JsonProperty("sunset") val sunset: Date? = null
    )

    data class CloudItem(
            @field:JsonProperty("all") val clouds: Int = 0
    )

    data class WeatherItem(
            @field:JsonProperty("id") val id: Int = 0,
            @field:JsonProperty("main") val main: String = "",
            @field:JsonProperty("description") val description: String = "",
            @field:JsonProperty("icon") val icon: String = ""
    )

    data class WeatherMainItem(
            @field:JsonProperty("temp") val temp: Double = 0.0,
            @field:JsonProperty("temp_min") val tempMin: Double = 0.0,
            @field:JsonProperty("temp_max") val tempMax: Double = 0.0,
            @field:JsonProperty("pressure") val pressure: Double = 0.0,
            @field:JsonProperty("sea_level") val seaLevel: Double = 0.0,
            @field:JsonProperty("grnd_level") val groundLevel: Double = 0.0,
            @field:JsonProperty("humidity") val humidity: Double = 0.0,
            @field:JsonProperty("temp_kf") val tempKf: Double = 0.0

    )

    data class WindItem(
            @field:JsonProperty("speed") val speed: Double = 0.0,
            @field:JsonProperty("deg") val deg: Double = 0.0
    )

}